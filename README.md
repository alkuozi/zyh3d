# 疫情数据实时监控
##预览
![封面](src/assets/QQ%E6%88%AA%E5%9B%BE20220506181913.png)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
