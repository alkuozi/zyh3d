import axios from 'axios'
import {base,wan,world,city} from './base.js'

const api = {
	// 病毒信息获取
	getInfo(){
		return axios.get(base.host+base.info)
	},
	getChina(){
		return axios.get(wan.host+wan.info)
	},
	getMock(){
		return axios.get('/chinaData')
	},
	getWold(){
		return axios.get(world.host+world.info)
	},
	getCity(city_name){
		let rules = md5(`appid15426city_name${city_name}formatjson9ca267eb7a6192d4f1dfa94847c9bbcf`);
		let canshu = `?format=json&appid15426&city_name${city_name}&sign${rules}`;
		return axios.get(city.host+city.info+canshu);
	},
	getSwiper(){
		return axios.get('http://iwenwiki.com/wapicovid19/ncovimg.php')
	},
	getBendi(){
		return axios.get('/api/pro')
	},
	getCur(){
		return axios.get('/api/cur')
	},
	getAll(){
		return axios.get('/api/all')
	}
}
export default api;
