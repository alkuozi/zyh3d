import Mock from 'mockjs';
const Random = Mock.Random;
const {pro,cur,all}=Mock.mock({
	// 累计确诊病例
	"pro":[
		{xArea: '西藏', confirm: '1'},
		{xArea: '青海', confirm: '45'},
		{xArea: '澳门', confirm: '82'},
		{xArea: '宁夏', confirm: '122'},
		{xArea: '贵州', confirm: '178'},
		{xArea: '海南', confirm: '283'},
		{xArea: '山西', confirm: '356'},
		{xArea: '甘肃', confirm: '681'},
		{xArea: '重庆', confirm: '693'},
		{xArea: '新疆', confirm: '999'},
		{xArea: '安徽', confirm: '1058'},
		{xArea: '江西', confirm: '1069'},
		{xArea: '湖南', confirm: '1337'},
		{xArea: '广西', confirm: '1538'},
		{xArea: '辽宁', confirm: '1608'},
		{xArea: '内蒙古', confirm: '1688'},
		{xArea: '天津', confirm: '1799'},
		{xArea: '北京', confirm: '1828'},
		{xArea: '四川', confirm: '1971'},
		{xArea: '河北', confirm: '1984'},
		{xArea: '云南', confirm: '2089'},
		{xArea: '江苏', confirm: '2113'},
		{xArea: '黑龙江', confirm: '2515'},
		{xArea: '山东', confirm: '2655'},
		{xArea: '河南', confirm: '2850'},
		{xArea: '浙江', confirm: '2859'},
		{xArea: '福建', confirm: '2942'},
		{xArea: '陕西', confirm: '3258'},
		{xArea: '广东', confirm: '6797'},
		{xArea: '上海', confirm: '17044'},
		{xArea: '台湾', confirm: '29593'},
		{xArea: '吉林', confirm: '38626'},
		{xArea: '湖北', confirm: '68394'}
	],
	"cur":[
		{xArea: '西藏', curConfirm: '0'},
		{xArea: '青海', curConfirm: '0'},
		{xArea: '台湾', curConfirm: '15978'},
		{xArea: '香港', curConfirm: '237615'},
		{xArea: '贵州', curConfirm: '4'},
		{xArea: '吉林', curConfirm: '11062'},
		{xArea: '新疆', curConfirm: '0'},
		{xArea: '宁夏', curConfirm: '0'},
		{xArea: '内蒙古', curConfirm: '8'},
		{xArea: '甘肃', curConfirm: '1'},
		{xArea: '天津', curConfirm: '9'},
		{xArea: '山西', curConfirm: '44'},
		{xArea: '辽宁', curConfirm: '40'},
		{xArea: '黑龙江', curConfirm: '100'},
		{xArea: '海南', curConfirm: '89'},
		{xArea: '河北', curConfirm: '45'},
		{xArea: '陕西', curConfirm: '34'},
		{xArea: '云南', curConfirm: '28'},
		{xArea: '广西', curConfirm: '78'},
		{xArea: '福建', curConfirm: '207'},
		{xArea: '上海', curConfirm: '9650'},
		{xArea: '北京', curConfirm: '68'},
		{xArea: '江苏', curConfirm: '89'},
		{xArea: '四川', curConfirm: '98'},
		{xArea: '山东', curConfirm: '176'},
		{xArea: '江西', curConfirm: '17'},
		{xArea: '重庆', curConfirm: '1'},
		{xArea: '安徽', curConfirm: '17'},
		{xArea: '湖南', curConfirm: '17'},
		{xArea: '河南', curConfirm: '27'},
		{xArea: '广东', curConfirm: '205'},
		{xArea: '浙江', curConfirm: '560'},
		{xArea: '湖北', curConfirm: '3'},
	],
	"all":{
		retdata:[
			{
				confirm:1,
				died:0,
				heal:1,
				asymptomatic:0,
				curConfirm:0,
				xArea:'西藏',
				subList:[
					{
						city:'拉萨',
						confirm:1,
						died:0,
						heal:1,
						curConfirm:0
					}
				]
			},
			{
				confirm:46,
				died:5,
				heal:46,
				asymptomatic:204,
				curConfirm:18,
				xArea:'澳门',
				subList:[]
			}
		]
	}
})
Mock.mock('/api/pro','get',(options)=>{
	return {
		status:200,
		message:'全国累计确诊病例',
		data:pro
	}
})
Mock.mock('/api/cur','get',(options)=>{
	return {
		status:200,
		message:'全国当前确诊病例',
		data:cur
	}
})
Mock.mock('/api/all','get',(options)=>{
	return {
		status:200,
		message:'全部的数据',
		data:all
	}
})