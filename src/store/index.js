import Vuex from 'vuex'
import Vue from 'vue'
import api from '@/api/index.js'
import * as echarts from 'echarts'
Vue.use(Vuex)

const actions ={
	china(context){
		api.getBendi().then(res=>{
			context.commit('CHINA',res)
		})
	},
	Cur(context){
		api.getCur().then(res=>{
			context.commit('CUR',res.data.data)
		})
	},
	All(context){
		api.getChina().then(res=>{
			context.commit('ALL',res.data.retdata);
			context.commit('TIME',res.data.relativeTime);
		})
	},
	
}
//准备mutations——用于操作数据
const mutations ={
	TIME(state,value){
		state.time = value
	},
	CHINA(state,value){
		const data = value.data.data.map((item)=>{
			return {
				name:item.xArea,
				value:item.confirm
			}
		})
		state.confirm = data;
	},
	CUR(state,value){
		let a =  value.sort((a,b)=>{
			return a.curConfirm -b.curConfirm
		})
		a= a.map((item)=>{
			return {
				name:item.xArea,
				value:item.curConfirm
			}
		})
		state.cur = a
	},
	ALL(state,value){
		//确诊病例
		let _data = value.map((item)=>{
			return {
				name:item.xArea,
				value:item.confirm
			}
		})
		_data =_data.sort((a,b)=>{
			return a.value - b.value
		})
		state.confirm = _data;
		//无证状
		let b =  value.sort((a,b)=>{
			return a.asymptomatic -b.asymptomatic
		})
		b= b.map((item)=>{
			return {
				name:item.xArea,
				value:item.asymptomatic
			}
		})
		state.asymptomatic = b;
		//地图
		let data = value.map((item)=>{
			return {
				name:item.xArea,
				value:item.curConfirm,
				// subList:item.subList.map(itemm=>{
				// 	return {
				// 		name:itemm.city,
				// 		value:itemm.curConfirm
				// 	}
				// })
			}
		})
		state.curConfirm = data;
		//死亡
		let a =  value.sort((a,b)=>{
			return a.died -b.died
		})
		a= a.map((item)=>{
			return {
				name:item.xArea,
				value:item.died
			}
		})
		state.died = a
		//饼图
		let arr = value.map(item=>{
			return {
				data:[
					{
						name:'无症状',
						value:item.asymptomatic
					},
					{
						name:'确认病例',
						value:item.confirm
					},
					{
						name:'当前确诊病例',
						value:item.curConfirm
					},
					{
						name:'死亡',
						value:item.died
					},
					{
						name:'治愈',
						value:item.heal
					}
				],
				name:item.xArea
			}
		})
		state.all = arr;
	},
	TIAN(state,value){
		state.tian = value;
	}
}

const getters={
	allInfo(state){
		let _all = [
			[
				{
					name:'累计确诊人数' +'\n'+'\n' + state.tian.desc.confirmedCount,
					value:state.tian.desc.confirmedCount ,//累计确诊人数
					itemStyle:{
						color:echarts.graphic.LinearGradient(0,1,0,0,[
							{
								offset:0,
								color:'#4ff778'
							},
							{
								offset:1,
								color:'#0ba82c'
							},
						])
					}
				},
				{ name:'现存确诊人数' +'\n'+'\n' + state.tian.desc.currentConfirmedCount,
					value:state.tian.desc.currentConfirmedCount  ,//现存确诊人数
					itemStyle:{
						color:'#333843'
					}
				}
			],
			
			[
				{ name:'相比昨天新增累计确诊人数'+'\n'+'\n' +state.tian.desc.yesterdayConfirmedCountIncr,
					value:state.tian.desc.yesterdayConfirmedCountIncr, //相比昨天新增累计确诊人数
					
				},
				{ name:'相比昨天现存确诊人数'+'\n'+'\n' +state.tian.desc.currentConfirmedIncr,
					value:state.tian.desc.currentConfirmedIncr,
					 itemStyle:{
					 	color:'#333843'
					 }//相比昨天现存确诊人数
				}
			],
			
			[
				{ name:'相比昨天境外输入确诊人数'+'\n'+'\n' +state.tian.desc.yesterdaySuspectedCountIncr,
					value:state.tian.desc.yesterdaySuspectedCountIncr,//相比昨天境外输入确诊人数
					
				},
				{ name:"新增境外输入人数"+'\n'+'\n' +state.tian.desc.suspectedIncr,
					value:state.tian.desc.suspectedIncr ,//	新增境外输入人数
					itemStyle:{
						color:'#333843'
					}
				}
			],
			[
				{
					name:'累计治愈人数'+'\n'+'\n' +state.tian.desc.curedCount,
					value:state.tian.desc.curedCount //累计治愈人数
				},
				{
					name:'累计死亡人数'+'\n'+'\n' +state.tian.desc.deadCount,
					value:state.tian.desc.deadCount ,//累计死亡人数
					itemStyle:{
						color:'#333843'
					}
				}
			],
			[
				{ name:'相比昨天新增治愈人数'+'\n'+'\n' +state.tian.desc.curedIncr,
					value:state.tian.desc.curedIncr//	相比昨天新增治愈人数
				},
				{ name:"相比昨天新增死亡人数"+'\n'+'\n' +state.tian.desc.deadIncr,
					value:state.tian.desc.deadIncr ,//相比昨天新增死亡人数
					itemStyle:{
						color:'#333843'
					}
				}
			],
			[
				{ name:'现存无症状人数'+'\n'+'\n' +state.tian.desc.seriousCount,
					value:state.tian.desc.seriousCount//现存无症状人数
				},
				{ name:'现存确诊人数'+'\n'+'\n' +state.tian.desc.currentConfirmedCount,
					value:state.tian.desc.currentConfirmedCount ,//	现存确诊人数
					itemStyle:{
						color:'#333843'
					}
				}
			],
			[
				{ name:'新增境外输入人数'+'\n'+'\n' +state.tian.desc.suspectedIncr,
					value:state.tian.desc.suspectedIncr//新增境外输入人数
				},
				{ name:'相比昨天境外输入确诊人数'+'\n'+'\n' +state.tian.desc.yesterdaySuspectedCountIncr,
					value:state.tian.desc.yesterdaySuspectedCountIncr ,//相比昨天境外输入确诊人数
					itemStyle:{
						color:'#333843'
					}
				}
			],
			[
				{ name:'国内高风险地区个数'+'\n'+'\n' +state.tian.desc.highDangerCount,
					value:state.tian.desc.highDangerCount//国内高风险地区个数
				},
				{ name:'国内中风险地区个数'+'\n'+'\n' +state.tian.desc.midDangerCount,
					value:state.tian.desc.midDangerCount ,//	国内中风险地区个数
					itemStyle:{
						color:'#333843'
					}
				}
			],
		]
		return _all
	}
}

//准备state——用于存储数据
const state={
	confirm:[],//全国确诊病例
	cur:[],//全国当前确诊病例
	all:{},//各省死亡、治愈等饼图
	tian:{},
	died:[],
	curConfirm:[],
	asymptomatic:[],
	time:''
}

const store = new Vuex.Store({
    actions,mutations,state,getters
})

export default store